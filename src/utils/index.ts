import { SampleType } from "../typing/interfaces";

export function getActivePath(pathname: string): string {
    return pathname.split('/').at(-1) || 'home';
}

export function generateSampleUrl(sample: SampleType) {
    if (sample.type === 'soundcloud') {
        return `https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/${sample.sample}&amp;color=8e8b8a&amp;inverse=true&amp;auto_play=false&amp;show_user=true`;
    } else if (sample.type === 'vimeo') {
        return `https://player.vimeo.com/video/${sample.sample}?byline=0&portrait=0`;
    } else if (sample.type === 'youtube') {
        return `https://www.youtube.com/embed/${sample.sample}?rel=0&amp;showinfo=0&autoplay=false`;
    }
    return '';
}
