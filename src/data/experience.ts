import { Employment } from "../typing/interfaces";
import { MusicExperiences } from "../typing/pages";

export const workExperience: Employment[] = [{
    "period": {
        "start": "2023-12-01",
        "end": "?"
    },
    "company": {
        "name": "Trackwell",
        "url": "http://trackwell.com"
    },
    "job": {
        "title": "Software engineer",
        "description": "Backend, frontend and devops"
    }
}, {
    "period": {
        "start": "2023-06-01",
        "end": "2023-12-01"
    },
    "company": {
        "name": "Controlant",
        "url": "http://controlant.com"
    },
    "job": {
        "title": "Software engineer",
        "description": "Backend, frontend, and CI/Terraform"
    }
}, {
    "period": {
        "start": "2017-01-16",
        "end": "2023-05-31"
    },
    "company": {
        "name": "Trackwell",
        "url": "http://trackwell.com"
    },
    "job": {
        "title": "Software engineer",
        "description": "Backend, frontend and devops"
    }
}, {
    "period": {
        "start": "2016-01-04",
        "end": "2017-01-13"
    },
    "company": {
        "name": "Directorate of Education in Iceland",
        "url": "https://mms.is"
    },
    "job": {
        "title": "Web engineer",
        "description": "Mostly frontend (websites, games and more) with a touch of backend/devops engineering."
    }
}, {
    "period": {
        "start": "2014-09-29",
        "end": "2015-11-16"
    },
    "company": {
        "name": "Plain Vanilla Games",
        "url": "http://www.quizup.com/"
    },
    "job": {
        "title": "Office IT Hacker",
        "description": "Sysadmin, office tech support, and various other projects."
    }
}, {
    "period": {
        "start": "2010-05-01",
        "end": "2012-05-01"
    },
    "company": {
        "name": "Penninn á Íslandi ehf.",
        "url": "http://penninn.is"
    },
    "job": {
        "title": "Store manager",
        "description": "Store manager and service representative at Eymundsson (book store) in Ísafjörður, Iceland."
    }
}, {
    "period": {
        "start": "2005-10-01",
        "end": "2010-05-01"
    },
    "company": {
        "name": "Penninn á Íslandi ehf.",
        "url": "http://penninn.is"
    },
    "job": {
        "title": "Customer service",
        "description": "Customer service in various bookstores in Reykjavík."
    }
}, {
    "period": {
        "start": "2004",
        "end": "2005"
    },
    "company": {
        "name": "Löndun ehf.",
        "url": "http://londun.is"
    },
    "job": {
        "title": "Worker",
        "description": "Offloading ships and counting."
    }
}, {
    "period": {
        "start": "2002",
        "end": "2004"
    },
    "company": {
        "name": "Klofningur ehf.",
        "url": "http://en.ja.is/klofningur-fiskthurrkun/"
    },
    "job": {
        "title": "Headcrusher",
        "description": "Crushing heads. Dried fishheads, to be precise."
    }
}];

export const musicExperience: MusicExperiences = {
    "theater": [{
        "id": "Dr. Faustus í myrku ljósi",
        "url": null,
        "date": "March, 2013",
        "place": "Reykjavík, Iceland",
        "description": "Music for a play, directed by Brynhildur Guðjónsdóttir for Menntaskólinn í Reykjavík.",
        "sample": {
            "sample": "80076092",
            "type": "soundcloud"
        }
    }, {
        "id": "Mamma",
        "url": null,
        "date": "August, 2012",
        "place": "Suðureyri, Iceland",
        "description": "Music for a modern dance piece by Anna Sigríður Ólafsdóttir.",
        "sample": {
            "sample": "55556182%3Fsecret_token%3Ds-8SKIz",
            "type": "soundcloud"
        }
    }, {
        "id": "Náströnd - Skáldið á Þröm",
        "url": null,
        "date": "March, 2012",
        "place": "Suðureyri, Iceland",
        "description": "Music for a monologue directed by Elfar Logi Hannesson.",
        "sample": {
            "sample": "39366846%3Fsecret_token%3Ds-fhVxb",
            "type": "soundcloud"
        }
    }, {
        "id": "White for Decay",
        "url": "http://id.is/white-for-decay/",
        "date": "March, 2011",
        "place": "Reykjavík, Iceland",
        "description": "Music and music box making for a modern dance piece choreographed by Sigríður Soffía Níelsdóttir for the Icelandic Dance Company. The video below uses the music from the piece, but is not related to the piece apart from the music.",
        "sample": {
            "sample": "kDN0rIyOKTs",
            "type": "youtube",
            "name": "Decaying World of White"
        }
    }, {
        "id": "Kyrrja",
        "url": null,
        "date": "May, 2010",
        "place": "Reykjavík, Iceland",
        "description": "Music for a modern dance piece by Ragnheiður Bjarnarson.",
        "sample": null,
    }, {
        "id": "Colorblind",
        "url": "http://siggasoffia.wordpress.com/colorblind/",
        "date": "June, 2010",
        "place": "Katowice, Poland",
        "description": "Music for a modern dance piece choreographed by Sigríður Soffía Níelsdóttir for the Silesian Dance Theater in Poland. The video below contains music from the piece, but is otherwise unrelated to the piece.",
        "sample": {
            "sample": "IdxXSe9NIVE",
            "type": "youtube",
            "name": "A Colorblind World"
        }
    }, {
        "id": "Lady's Choice",
        "url": "http://siggasoffia.wordpress.com/lady%C2%B4s-choice/",
        "date": "February, 2009",
        "place": "Reykjavík, Iceland",
        "description": "Music for a modern dance piece choreographed by Sigríður Soffía Níelsdóttir.",
        "sample": {
            "sample": "1013949",
            "type": "soundcloud"
        }
    }, {
        "id": "Kirsuberjagarðurinn",
        "url": null,
        "date": "December, 2008",
        "place": "Reykjavík, Iceland",
        "description": "Music for the play \"The Cherry Orchard\", by Anton Chekov, directed by Daniel Rylander for the Iceland Academy of the Arts Student Theatre.",
        "sample": {
            "sample": "25049592",
            "type": "soundcloud"
        }
    }, {
        "id": "Hunt me down",
        "url": "http://www.linnealindh.com/huntmedown.html",
        "date": "June, 2008",
        "place": "Copenhagen, Denmark",
        "description": "Music for a modern dance piece choreographed by Linnea Lindh. The music was also used for a video based on the piece.",
        "sample": {
            "sample": "25629698",
            "type": "vimeo",
            "name": "Hunt me down"
        }
    }, {
        "id": "Skugga Sveinn",
        "url": null,
        "date": "November, 2007",
        "place": "Ísafjörður, Iceland",
        "description": "Music for a play",
        "sample": null,
    }, {
        "id": "Hver um sig",
        "url": "http://id.is/hver-um-sig/",
        "date": "Fall, 2006",
        "place": "Reykjavík, Iceland",
        "description": "Music composed with Valdimar Jóhannsson for a modern dance piece choreographed by Vaðall fo the Icelandic Dance Company.",
        "sample": null,
    }, {
        "id": "Flest um fátt",
        "url": null,
        "date": "April, 2006",
        "place": "Reykjavík, Iceland",
        "description": "Music composed with Valdimar Jóhannsson for a modern dance piece choreographed by Valgerður Rúnarsdóttir and Aðalheiður Halldórsdóttir",
        "sample": null,
    }, {
        "id": "Bróðir minn Ljónshjarta",
        "url": null,
        "date": "July, 2004",
        "place": "Suðureyri, Iceland",
        "description": "Music for the play \"My Brother the Lionheart\", by Astrid Lindgren.",
        "sample": null,
    }],
    "film": [{
        "id": "Dýrafjörður",
        "url": "https://www.reelhouse.org/stonekeyfilms/dyrafjordur",
        "date": "February, 2014",
        "place": "Dýrafjörður",
        "description": "Music for a full feature documentary directed by Philip Carrel",
        "sample": null,
    }, {
        "id": "Requiem",
        "url": "https://youtu.be/qkZFT7atWu8",
        "date": "August, 2013",
        "place": "Reykjavík, Iceland",
        "description": "Music for a short modern dance video by Sigríður Soffía Níelsdóttir and Marínó Thorlacius",
        "sample": {
            "sample": "qkZFT7atWu8",
            "type": "youtube",
            "name": "Requiem"
        }
    }, {
        "id": "Kuml",
        "url": null,
        "date": "May, 2013",
        "place": "Reykjavík, Iceland",
        "description": "Music for a short thriller by Gláma productions",
        "sample": {
            "sample": "94738761%3Fsecret_token%3Ds-C2IgL",
            "type": "soundcloud"
        }
    }, {
        "id": "Folded",
        "url": "http://www.linnealindh.com/folded.html",
        "date": "November, 2011",
        "place": "Stockholm",
        "description": "Music for a modern dance video choreographed by Linnea Lindh for an exhibition in Gallery Detroit, Stockholm.",
        "sample": {
            "sample": "34710637",
            "type": "vimeo",
            "name": "Folded"
        }
    }, {
        "id": "Lady's Choice",
        "url": null,
        "date": "May, 2009",
        "place": "Reykjavík, Iceland",
        "description": "Music and video editing for the modern dance piece \"Lady's Choice\" choreographed by Sigríður Soffía Níelsdóttir.",
        "sample": {
            "sample": "1013949",
            "type": "soundcloud"
        }
    }, {
        "id": "Krossgötur",
        "url": null,
        "date": "May, 2008",
        "place": "Reykjavík, Iceland",
        "description": "Music for a film directed by Ragnar Bragason for th Iceland Academy of the Arts Student theatre",
        "sample": null,
    }, {
        "id": "Uniform Sierra",
        "url": "http://siggasoffia.wordpress.com/uniform-sierradanceshortfilm/",
        "date": "May, 2008",
        "place": "Reykjavík, Iceland",
        "description": "Music for a modern dance video by Sigríður Soffía Níelsdóttir.",
        "sample": null,
    }, {
        "id": "Irresistible fetus",
        "url": null,
        "date": "May, 2008",
        "place": "Reykjavík, Iceland",
        "description": "Music for a modern dance video by Ragnheiður Bjarnason",
        "sample": null,
    }, {
        "id": "Óshlíð",
        "url": null,
        "date": "March, 2008",
        "place": "Reykjavík, Iceland",
        "description": "Music for a short documentary",
        "sample": null,
    }],
    "recordings": [{
        "id": "Stories of Meanwhile",
        "url": "https://soundcloud.com/7oi/sets/stories-of-meanwhile",
        "date": "2010",
        "place": "Ísafjörður, Iceland",
        "description": "An album made as the artist 7oi",
        "sample": null,
    }, {
        "id": "Itemhljóð og Veinan",
        "url": "https://open.spotify.com/album/3GScxGiIanGaCzMLsZbLRK",
        "date": "2009",
        "place": "Reykjavík, Iceland",
        "description": "Recording and mixing of an album by AMFJ, released on cassette and digitally.",
        "sample": null,
    }, {
        "id": "Don't Push The Rocks In My Face",
        "url": "https://open.spotify.com/album/0CA1DH4u2ZjEs0WmistRQB",
        "date": "2009",
        "place": "Reykjavík, Iceland",
        "description": "An album made as the artist 7oi",
        "sample": null,
    }, {
        "id": "Lost Under A Pile",
        "url": "",
        "date": "2007",
        "place": "Ísafjörður, Iceland",
        "description": "An album made as the artist 7oi",
        "sample": null,
    }, {
        "id": "Þið eruð ekki að taka mig nógu bókstaflega",
        "url": "https://open.spotify.com/album/61CmhPMuvOqTnB0k1YMZny",
        "date": "2003",
        "place": "Reykjavík, Iceland",
        "description": "An album made as the artist 7oi",
        "sample": null,
    }, {
        "id": "Allt virðist svo smátt séð héðan frá",
        "url": "https://open.spotify.com/album/1H8BbmTfh3DYgJNUEonN8W",
        "date": "2002",
        "place": "Ísafjörður, Iceland",
        "description": "An album made as the artist 7oi",
        "sample": null,
    }, {
        "id": "Síngur",
        "url": "https://open.spotify.com/album/6WPLm5ACygfYUtrQflawAQ",
        "date": "2001",
        "place": "Ísafjörður, Iceland",
        "description": "An album made as the artist 7oi",
        "sample": null,
    }, {
        "id": "0",
        "url": null,
        "date": "2000",
        "place": "Ísafjörður, Iceland",
        "description": "An album made as the artist 7oi",
        "sample": null,
    }],
    "oddities": [{
        "id": "Eldar",
        "url": null,
        "date": "August, 2013",
        "place": "Reykjavík, Iceland",
        "description": "A drone for a fireworks display, choreographed by Sigríður Soffía Níelsdóttir.",
        "sample": {
            "sample": "106826940",
            "type": "soundcloud"
        }
    }, {
        "id": "Soup Stock Tokyo",
        "url": "http://icelandreview.com/news/2008/12/05/soup-stock-tokyo-offers-icelandic-lobster-soup",
        "date": "November, 2008",
        "place": "Japan",
        "description": "A set of my music put together to be played in around 50 Soup Stock Tokyo restaurants around Japan.",
        "sample": {
            "sample": "855043",
            "type": "soundcloud"
        }
    }]
};
