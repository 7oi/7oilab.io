import { Command, CommandMap } from "../typing/commands";

export const homeCommand: Command = {
    "cmd": "home",
    "info": "Go back to home screen",
    "path": "/",
};

export const defaultCommands: Command[] = [
    homeCommand,
]

export const homeCommands: Command[] = [
    {
        "cmd": "about",
        "info": "Shows more about me, such as interests, backstory, etc.",
        "path": "/about",
    },
    {
        "cmd": "education",
        "info": "Shows my education",
        "path": "/education",
    },
    {
        "cmd": "skills",
        "info": "Lists my skills.",
        "path": "/skills",
    },
    {
        "cmd": "experience",
        "info": "Lists my work and music making experience",
        "path": "/experience",
    },
    {
        "cmd": "references",
        "info": "Lists my references",
        "path": "/references",
    }
];

export const aboutCommands: Command[] = [
    // {
    //     "cmd": "more",
    //     "info": "More about me",
    //     "path": "/about/more",
    // },
    {
        "cmd": "interests",
        "info": "My interest",
        "path": "/about/interests",
    },
    homeCommand
];

export const skillCommands: Command[] = [
    {
        "cmd": "programming",
        "info": "Programming languages",
        "path": "/skills/programming",
    },
    {
        "cmd": "tools",
        "info": "Tools I use",
        "path": "/skills/tools",
    },
    {
        "cmd": "software",
        "info": "Software I use and/or love",
        "path": "/skills/software",
    },
    {
        "cmd": "os",
        "info": "Operating systems I've used",
        "path": "/skills/os",
    },
    homeCommand,
];

export const experienceCommands: Command[] = [
    {
        "cmd": "work",
        "info": "My work experience",
        "path": "/experience/work",
    },
    {
        "cmd": "music",
        "info": "My musical experience",
        "path": "/experience/music"
    },
    homeCommand
];

export const musicCommands: Command[] = [
    {
        "cmd": "theater",
        "info": "Works I've done for theater",
        "path": "/experience/music",
    },
    {
        "cmd": "film",
        "info": "Works I've done for films",
        "path": "/experience/music",
    },
    {
        "cmd": "recordings",
        "info": "Albums I've produced, both my own and other",
        "path": "/experience/music",
    },
    {
        "cmd": "oddities",
        "info": "Things that don't quite fit in",
        "path": "/experience/music",
    },
    {
        "cmd": "back",
        "info": "Back to experiences",
        "path": "/experience",
    },
    homeCommand
];

export const allCommands: string[] = homeCommands.map(c => c.cmd).concat(
    aboutCommands.map(c => c.cmd),
    skillCommands.map(c => c.cmd),
    experienceCommands.map(c => c.cmd),
    musicCommands.map(c => c.cmd)
);

export const hiddenCommands: string[] = ['stahp', 'stop', 'freakout', 'disco', 'cd ~', 'exit'];

export const commandMap: CommandMap = {
    home: homeCommands,
    about: aboutCommands,
    skills: skillCommands,
    education: defaultCommands,
    references: defaultCommands,
    experience: experienceCommands,
    music: musicCommands,
};
