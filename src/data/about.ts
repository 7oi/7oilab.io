import { AboutPage } from "../typing/pages";

export const about: AboutPage = {
    "name": "Jóhann Friðgeir Jóhannsson",
    "phone": "+354-865-6489",
    "email": "7oi@7oi.is",
    "about": "I am born and raised in Ísafjörður, a small rural town in the Westfjords of Iceland, where I live today. I'm married with four kids (yes four) born in 2007, 2009, 2016 and 2020. My wife is an artist from Serbia and one of our biggest dreams is to have a house in the countryside and live off the land and do what we love, ie. program, make music and art.",
    "qualities": "I have always been a hard worker. I'm a really quick learner and don't need long to adapt to new environments. I am also very stubborn and don't give up if I hit walls. I am, though, quite realistic as well, so I can very well realize if the wall is worth breaking through.",
    "status": "Employed",
    "links": [{
        "id": "GitHub",
        "url": "https://github.com/7oi"
    }, {
        "id": "Gitlab",
        "url": "https://gitlab.com/7oi",
    }, {
        "id": "LinkedIn",
        "url": "https://www.linkedin.com/in/7oi/"
    }, {
        "id": "Soundcloud",
        "url": "http://soundcloud.com/7oi"
    }, {
        "id": "YouTube",
        "url": "https://www.youtube.com/user/dextroc/videos"
    }],
    "interests": [{
        "id": "Music",
        "details": "is everything from a hobby to a passion to my job, when I have paid projects (which come along and are mostly modern dance related)."
    }, {
        "id": "Programming",
        "details": "could have the same description as music, really. My brain always goes into programming mode in my daily life when trying to figure out various problems. Music-related programming and web-programming are most interesting for me (especially music programming, as it combines two passions) but I also enjoy improving my efficiency with scripting etc. I've also been quite attracted to more visual- and game-programming lately."
    }, {
        "id": "Animation",
        "details": "has a special place in my mind. I find that a well animated and written cartoon can be a very rewarding and creative experience to watch."
    }, {
        "id": "Comics",
        "details": "are also a favourite, whether it's reading them or writing small and stupid ones."
    }, {
        "id": "Electronics",
        "details": "have \"sparked\" my interest for a while and I love messing around with Arduinos."
    }, {
        "id": "Making musical instruments",
        "details": "is fun and I have made a few hank drums and weird controllers."
    }, {
        "id": "Tech geekiness",
        "details": "and especially related to music, is always something I keep track of."
    }]
};
