import { SkillsList } from "../typing/pages";

export const skillNote: string = "These are my skills. I rate my skills in each one between 0-10. I am not good at estimating how well I know them, though, so I might over- or underestimate, rendering the rating a bit pointless...";

export const skills: SkillsList = {
    "programming": [{
        "id": "Python",
        "score": "8",
        "use": "A lot",
        "details": "I've used Python a lot, whether it's been for making small web services, scrapers, Django projects, and more. I've become especially familiar with Django and DRF as I have experience with it since Django 1.6"
    }, {
        "id": "JavaScript",
        "score": "8",
        "use": "Frequently",
        "details": "My first love. I'v most recently been using it to create React apps (example: this CV, which was updated to latest React version as of March 2023, along with converted to TypeScript)."
    }, {
        "id": "CSS",
        "score": "8",
        "use": "Frequently",
        "details": "Hardly worth bringing up. Of course I know CSS. Although I rather hover towards SASS. Much cleaner."
    }, {
        "id": "Shell scripting",
        "score": "7",
        "use": "Sometimes",
        "details": "I love spending time in the shell (mostly zsh with oh-my-zsh). Although Shell scripts should probably be avoided professionally, I cannot resist using them personally."
    }, {
        "id": "C",
        "score": "5",
        "use": "Rarely",
        "details": "I have not used C since I was in university. I enjoyed it immensely, though, even though it can mess you up."
    }, {
        "id": "C++",
        "score": "5",
        "use": "Rarely",
        "details": "Again, have not used since university, apart from one test for a job application. I want to get into it again one day, for creating music software/plugins."
    }, {
        "id": "Java",
        "score": "5",
        "use": "Rarely",
        "details": "Have not used since university. Did quite well with Java there, though."
    }, {
        "id": "Others",
        "score": "3-7",
        "use": "Rarely",
        "details": "Other languages I studied in school that are not completely foreign to me: C# (.Net), Scheme, SML, Prolog..."
    }],
    "tools": [{
        "id": "Git",
        "score": "8",
        "use": "Constantly",
        "details": "Where would I be without it? I love diving into git. It's so much more powerful than for just pushing and pulling."
    }, {
        "id": "VS Code",
        "score": "8",
        "use": "A lot",
        "details": "My go to code editor. Jumped ship from Sublime and have been enjoing setting up dev containers for isolated project workspaces within docker containers."
    }, {
        "id": "Docker",
        "score": "8",
        "use": "A lot",
        "details": "I have explored docker containers for a while and enjoy it a lot. I basically start all projects by containerizing them and setting up a convenient docker-compose setup for development purposes."
    }, {
        "id": "AWS",
        "score": "6-7",
        "use": "Frequently",
        "details": "I have attended a few courses and played around AWS a lot. I am quite familiar with IAM (minimal possible permissions all the way), EC2, S3, SNS, RDS and such, but I still have a lot to learn. I plan on getting certified and will update this space when I do."
    }, {
        "id": "Gitlab CI",
        "score": "8",
        "use": "A lot",
        "details": "I basically became the go to guy for Gitlab CI at Trackwell. I use it a lot."
    }, {
        "id": "Jenkins CI",
        "score": "4-7",
        "use": "Rarely",
        "details": "I set up Jenkins for our final project at university. Grading yourself in Jenkins knowledge is impossible. There's always something you've missed."
    }, {
        "id": "Homebrew",
        "score": "6",
        "use": "Frequently",
        "details": "Essential to be able to do anything on the Mac. Coupled with Cask, I hardly need anything more than the terminal. I have made a few recipes myself for personal use."
    }, {
        "id": "Shells",
        "score": "8",
        "use": "Always",
        "details": "I live in the shell. You can do anything there, SSH into machines, build apps, networking, downloading, and the list goes on. And lots of flavours available (zsh, bash, fish, etc)."
    }, {
        "id": "Nginx",
        "score": "7",
        "use": "Frequently",
        "details": "I like to use nginx to serve my sites. Highly configurable in an understandable manner, once you get used to it."
    }],
    "os": [{
        "id": "MacOS",
        "score": "7.5",
        "use": "Frequently",
        "details": "I used to consider myself almost an expert on MacOS/OSX, but have become a bit rusty. I have dug into it, even the OSX Server part, and I could possibly troubleshoot most problems that come up. Although, it still manages to come up with undecipherable problems from time to time."
    }, {
        "id": "Windows",
        "score": "8",
        "use": "Rarely",
        "details": "I grew up on Windows, like most people. I've broken it many times trying to bend it to my will."
    }, {
        "id": "Linux",
        "score": "8",
        "use": "Often",
        "details": "I've worked with many distros and mostly just hover towards arch for my personal setup."
    }],
    "software": [{
        "id": "Many",
        "score": "...",
        "use": "Frequently",
        "details": "I don't actually see much point in listing up software software I use anymore. Not really the most useful thing to know, is it?"
    }]
};
