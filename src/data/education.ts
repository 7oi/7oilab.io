import { Education } from "../typing/interfaces";

export const education: Education[] = [
    {
        "degree": "BSc",
        "program": "Computer Science",
        "school": {
            "name": "Reykjavík University",
            "url": "http://www.ru.is"
        },
        "period": {
            "start": "2012",
            "end": "2014"
        },
        "average": "8.55",
        "status": "Finished",
        "final_project": {
            "title": "Responsive ad manager",
            "description": "A web-based solution programmed in Python (Django) to build responsive ads for the web. Built for a company called Hugsmiðjan in a group with Arnór Bogason og Andri Ólafsson.",
            "instructor": "Elín Elísabet Torfadóttir",
            "grade": "9"
        }
    }, {
        "degree": "BA",
        "program": "New media in music composition",
        "school": {
            "name": "Iceland Academy of the Arts",
            "url": "http://www.lhi.is"
        },
        "period": {
            "start": "2006",
            "end": "2009"
        },
        "average": "8.5",
        "status": "BA essay unfinished",
        "final_project": {
            "title": "Lady’s Choice",
            "description": "Music and video editing for the modern dance piece \"Lady's Choice\" by Sigríður Soffía Níelsdóttir",
            "instructor": "Ríkharður H. Friðriksson",
            "grade": "7.5"
        }
    }
];
