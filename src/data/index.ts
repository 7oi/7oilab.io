import { education } from "./education";
import { about } from './about';
import { skillNote, skills } from './skills';
import { workExperience, musicExperience } from './experience';
import lemon from "./asciiArt/Lemongrab";
import pic from "./asciiArt/7oi";
import { status } from './data';

export {
    about,
    education,
    lemon,
    musicExperience,
    pic,
    skillNote,
    skills,
    status,
    workExperience
};
