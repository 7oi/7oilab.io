/*reducers.ts*/

import { SampleType } from "./typing/interfaces";
import { Command } from './typing/commands';

type ActionMap<M extends { [index: string]: any }> = {
    [Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key;
    }
    : {
        type: Key;
        payload: M[Key];
    }
};

type StateType = {
    showModal: boolean,
    sample: SampleType | null,
    command: Command,
}

export enum Types {
    ShowModal = 'SHOW_MODAL',
    SetSample = 'SET_SAMPLE',
    SetCommand = 'SET_COMMAND',
}

type Payload = {
    [Types.ShowModal]: boolean,
    [Types.SetSample]: SampleType | null,
    [Types.SetCommand]: Command,
}

export type Actions = ActionMap<Payload>[keyof ActionMap<Payload>];


export const appReducer = (state: StateType, action: Actions) => {
    switch (action.type) {
        case Types.ShowModal:
            return {
                ...state,
                showModal: action.payload,
            }
        case Types.SetSample:
            return {
                ...state,
                sample: action.payload,
            }
        case Types.SetCommand:
            return {
                ...state,
                command: action.payload,
            }
        default:
            return state;
    }
}
