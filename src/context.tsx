import React, { createContext, useReducer, Dispatch } from 'react';
import { appReducer, Actions } from './reducers';
import { SampleType } from './typing/interfaces';
import { Command } from './typing/commands';
import { homeCommand } from './data/commands';

type InitialStateType = {
    showModal: boolean,
    sample: SampleType | null,
    command: Command,
}

interface Props {
    children: React.ReactNode;
}

const initialstate = {
    showModal: false,
    sample: null,
    command: homeCommand,
};

const AppContext = createContext<{
    state: InitialStateType;
    dispatch: Dispatch<Actions>;
}>({ state: initialstate, dispatch: () => null });


const AppProvider: React.FC<Props> = ({ children }) => {
    const [state, dispatch] = useReducer(appReducer, initialstate);
    return (
        <AppContext.Provider value={{ state, dispatch}}>
            {children}
        </AppContext.Provider>
    )
}

export { AppContext, AppProvider };
