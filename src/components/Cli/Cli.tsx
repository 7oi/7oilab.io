import React, { useContext, useEffect, useRef, useCallback, useState, useMemo } from 'react';
import { Link } from 'react-router-dom';
import { WindupChildren, Pace, Pause } from 'windups';
import './Cli.scss';
import {
  homeCommands,
  allCommands,
  hiddenCommands,
  commandMap,
} from '../../data/commands';
import {Row, Col} from 'react-bootstrap';
import { AppContext } from '../../context';
import { MAIN_COLOR } from '../../constants';
import { Types } from '../../reducers';
import { Outlet, useLocation, useNavigate } from 'react-router';
import { Timer } from '../../typing/types';
import { CommandMap } from '../../typing/commands';
import { Header } from '../Header';
import Footer from '../Footer';
import { getActivePath } from '../../utils';

function Cli() {
  const { state, dispatch } = useContext(AppContext);
  const { command } = state;
  const location = useLocation();
  const navigate = useNavigate();


  const discoInterval = useRef<Timer>();
  const [disco, setDisco] = useState<boolean>(false);
  const [focus, setFocus] = useState<number>(0);
  const [tabs, setTabs] = useState<number>(0);
  const [suggest, setSuggest] = useState<string[]>([]);
  const [error, setError] = useState<string | null>(null);
  const [activeCommandsKey, setActiveCommandsKey] = useState<string>(() => getActivePath(location.pathname));

  const focusOnCmd = useCallback((e: KeyboardEvent) => {
    if (e.key === 'ArrowUp' || e.key === 'ArrowDown') {
      let newFocus = focus;
      let count = document.getElementsByTagName('tbody').length;
      if (e.key === 'ArrowUp') {
        newFocus = focus <= 0 ? count : focus - 1;
      }
      else if (e.key === 'ArrowDown') {
        newFocus = focus >= count - 1 ? 0 : focus + 1;
      }
      setFocus(newFocus);
      document.getElementsByTagName('tbody')[newFocus].scrollIntoView();
    }
  }, [focus]);

  useEffect(() => {
    document.addEventListener("keydown", focusOnCmd, false);
    return () => {
      document.removeEventListener("keydown", focusOnCmd);
    }
  }, [focusOnCmd]);

  useEffect(() => {
    // Don't look at this. This is a secret.
    if (disco) {
      let r = 0;
      let b = 0;
      let g = 0;
      discoInterval.current = setInterval(() => {
        r = r <= 1 ? 255-r : r-2;
        b = b >= 255 ? 0 : b+1;
        g = g <= 0 ? 255 : g-1;

        const r_hex = r < 10 ? '0' + r.toString(16) : r.toString(16);
        const g_hex = g < 10 ? '0' + g.toString(16) : g.toString(16);
        const b_hex = b < 10 ? '0' + b.toString(16) : b.toString(16);

        const bg_color = '#' + r_hex + b_hex + g_hex;
        const color    = '#' + g_hex + r_hex + b_hex;

        document.body.style.backgroundColor = bg_color;
        document.body.style.color           = color;
      }, 4);
    }
    return () => {
      clearInterval(discoInterval.current);
      document.body.style.backgroundColor = "#000000";
      document.body.style.color = MAIN_COLOR;
    }
  }, [disco]);

  useEffect(() => {
    let activePath = getActivePath(location.pathname);
    if (activePath === activeCommandsKey) return;
    if (Object.keys(commandMap).indexOf(activePath) === -1) {
      return;
    }
    setActiveCommandsKey(activePath);
  }, [location.pathname, activeCommandsKey])

  const commands = useMemo(() => {
    if (!activeCommandsKey) return homeCommands;
    return commandMap[activeCommandsKey as keyof CommandMap];
  }, [activeCommandsKey]);

  useEffect(() => {

    switch(command.cmd) {
      case 'disco':
      case 'freakout':
        setDisco(true);
        break;
      case 'stahp':
      case 'kill':
      case 'stop':
        setDisco(false);
        break;
      default:
        break;
    }
    setError(null);
  }, [command.cmd]);

  const tabComplete = useCallback((event: React.KeyboardEvent<HTMLInputElement>, input: string) => {
    event.preventDefault();
    if (!input) {
      setTabs(0);
      setSuggest([]);
      return;
    }
    const suggestions = allCommands.filter(cmd => cmd.startsWith(input));
    if (suggestions.length === 0) {
      setTabs(0);
      setSuggest([]);
      return;
    } else if (suggestions.length === 1) {
      setTabs(0);
      event.currentTarget.value = suggestions[0];
      return;
    }
    const tabCount = (suggestions.length <= tabs) ? 0 : tabs + 1;
    setSuggest(suggestions);
    setTabs(tabCount);
  }, [tabs]);

  const selectedSuggestion = useMemo(() => suggest[tabs] || null, [tabs, suggest]);

  const enterCmd = useCallback((event: React.KeyboardEvent<HTMLInputElement>) => {
    const input = event.currentTarget.value.toLowerCase().trim();
    if (event.key === 'Enter') {
      setError(null);
      let cmd = (selectedSuggestion) ? selectedSuggestion : input;
      if (hiddenCommands.indexOf(cmd) >= 0) {
        switch (cmd) {
          case 'disco':
          case 'freakout':
            setDisco(true);
            event.currentTarget.value = '';
            break;
          case 'stahp':
          case 'kill':
          case 'stop':
            setDisco(false);
            event.currentTarget.value = '';
            break;
          case 'cd ~':
            cmd = 'home';
            break;
          default:
            break;
        }
        return;
      }

      const runCmd = commands.find(c => c.cmd === cmd);
      if (runCmd) {
        dispatch({ type: Types.SetCommand, payload: runCmd });
        navigate(runCmd.path);
      } else {
        setError(`Error: Command ${cmd} not found`);
      }
      event.currentTarget.value = '';
    } else if (event.key === 'Tab') {
      tabComplete(event, input);
    } else {
      setError(null);
    }
  }, [selectedSuggestion, tabComplete, commands, dispatch, navigate]);

  const cmds = useMemo(() => {
    if (!commands) return null;
    return (
      <Pace ms={2}>
        {commands.map(cmd => (
          <tr key={cmd.cmd}>
            <td>
              {<Link to={cmd.path} onClick={() => dispatch({ type: Types.SetCommand, payload: cmd })}>{cmd.cmd}</Link>}
            </td>
            <td>
              {cmd.info}
            </td>
            <Pause ms={200} />
          </tr>
        ))}
      </Pace>
    )
  }, [commands, dispatch]);

  return (
    <>
      <Header />
      <Row className="Cli">
        <Col xs={12}>
        <hr />
          <p>Commands:</p>
          <table>
            <tbody className="commands">
              <WindupChildren>{cmds}</WindupChildren>
            </tbody>
          </table>
        </Col>
        <Col xs={12}>
          {error && <p className="error">{error}</p>}
          <span>{`~${location.pathname} $ `}</span>
          <input type="text" className="cmd" style={{ color: MAIN_COLOR }} autoFocus onKeyDown={(e) => enterCmd(e)} />
          <div>{suggest.map((s, i) => (
            <span key={i} className={(tabs === i) ? 'suggestion active' : 'suggestion'}>{s}</span>
          ))}</div>
        </Col>
        <Col xs={12} className="cv-content">
          <Outlet />
        </Col>
      </Row>
      <Footer />
    </>
  );
}

export default Cli;
