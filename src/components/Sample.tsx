import React, {  useContext } from 'react';
import { AppContext } from '../context';
import { SampleType } from '../typing/interfaces';
import { Types } from '../reducers';

interface SampleProps {
	sample: SampleType | null
}


function Sample({ sample }:SampleProps) {
	const { dispatch } = useContext(AppContext);
	const setSample = (sample: SampleType) => {
		dispatch({ type: Types.SetSample, payload: sample });
		if (sample.type !== 'soundcloud') {
			dispatch({ type: Types.ShowModal, payload: true });
		}
	}

	if (sample) {
		return (<tr>
			<td>Sample      : </td>
			<td>
				<button className="sample triggerbtn" onClick={() => setSample(sample)}>
					{sample?.type}
				</button>
			</td>
		</tr>);
	}
	return null;
}
export default Sample;
