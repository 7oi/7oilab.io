import React, { useMemo, useContext } from 'react';
import Modal from 'react-modal';
import { AppContext } from '../../context';
import { Types } from '../../reducers';
import { generateSampleUrl } from '../../utils';
import './Footer.scss';

const modalStyle: Modal.Styles = {
	overlay: {
		position: 'fixed',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		backgroundColor: 'rgba(0, 0, 0, 0.5)'
	},
	content: {
		position: 'relative',
		display: 'flex',
		justifyContent: 'center',
		width: '100%',
		border: 'none',
		background: 'rgba(0, 0, 0, 0.5)',
		overflow: 'auto',
		WebkitOverflowScrolling: 'touch',
		borderRadius: '4px',
		outline: 'none',
		padding: '20px'
	}
};


function Footer() {
	const { state, dispatch } = useContext(AppContext);
	const { showModal, sample } = state;

	const renderedSample = useMemo(() => {
		const toggleSample = (on = true) => {
			dispatch({ type: Types.ShowModal, payload: on });
		};

		if (sample === null) return null;

		let url: string = generateSampleUrl(sample);

		if (sample.type === 'soundcloud') {
			return (
				<iframe
					title="soundcloud_sample"
					width="100%"
					height="20"
					scrolling="no"
					frameBorder="no"
					src={url}
				></iframe>
			);
		}
		return (
			<div className='Footer'>
				<button
					className="sample triggerbtn"
					onClick={() => toggleSample(true)}
				>
					View {sample.name || sample.sample}
				</button>
				<Modal isOpen={showModal} onRequestClose={() => toggleSample(false)} style={modalStyle}>
					<iframe title="sample_video" src={url} frameBorder="0" allowFullScreen></iframe>
					<button
						className="sample triggerbtn"
						onClick={() => toggleSample(false)}
					>
						Close
					</button>
				</Modal>
			</div>
		)
	}, [sample, showModal, dispatch]);

	return (
		<div className="footer">{renderedSample}</div>
	)
}

export default Footer;
