import React, { useCallback, useContext, useState } from 'react';
import './Picture.scss';
import { pic, lemon } from '../../data/asciiArt';
import { WindupChildren, Pace } from 'windups';
import { MAIN_COLOR, WAT } from '../../constants';
import { AppContext } from '../../context';
import { Types } from '../../reducers';

interface PictureProps {
  skipped?: boolean
}


function Picture({ skipped }:PictureProps) {
  const { dispatch } = useContext(AppContext);

  const [clicks, setClicks] = useState(0);
  const [picture, setPicture] = useState(pic);
  const [color, setColor] = useState(MAIN_COLOR);

  const dontClickMe = useCallback(() => {
    let newClicks = clicks + 1;

    if (newClicks === 4) {
      console.log('Stop it!');
    } else if (newClicks === 7) {
      console.log('I mean it!');
    } else if (newClicks === 13) {
      dispatch({ type: Types.SetSample, payload: WAT });
      dispatch({ type: Types.ShowModal, payload: true });
      setPicture(lemon);
      setColor('yellow');
      newClicks = 0;
    }
    setClicks(newClicks);
  }, [clicks, dispatch]);

  return (
    <p className="me-pic" onClick={() => dontClickMe()} style={{ color }}>
      <WindupChildren skipped={skipped}>
        <Pace ms={1}>
          {picture}
        </Pace>
      </WindupChildren>
    </p>
  );
}

export default Picture;
