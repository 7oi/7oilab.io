import React, { useCallback, useMemo, useRef } from 'react';
import './Header.scss';
import Picture    from './Picture';
import { about, status } from '../../data';
import { WindupChildren, Pace } from 'windups';
import { useLocation } from 'react-router';
import { MyCV } from './MyCv';


function Header() {
  const loading = useRef<boolean>(true);
  const location = useLocation();

  const skipTyping = useMemo(() => {
    const currPath = location.pathname.split('/').at(-1) || 'home';
    const isLoading = loading.current === true;
    if ((!isLoading && currPath === 'home') || currPath !== 'home') {
      loading.current = false;
      return true;
    }
    return false;
  }, [location.pathname]);

  const doneTyping = useCallback(() => {
    if (loading.current === true) {
      loading.current = false;
    }
  }, []);

  const hiddenGem = () => {
    console.log(`Whatever you do, don't type the "disco" command in. It might not stahp.`);
  };

  return (
    <div className="Header">
      {status && <div className="statusMessage">{status}</div>}
      <WindupChildren onFinished={() => doneTyping()} skipped={skipTyping} >
        <Pace ms={10}>
          <div className='info'>
            <p className="heading hidden-xs"><MyCV hiddenFunc={hiddenGem} skipped={skipTyping} /></p>
            <table className="ascii-table">
              <tbody>
                <tr>
                  <td>Name  : </td>
                  <td>{about.name}</td>
                </tr>
                <tr>
                  <td>Phone : </td>
                  <td><a href={`tel:${about.phone}`}>{about.phone}</a></td>
                </tr>
                <tr>
                  <td>Email : </td>
                  <td><a href={`mailto:${about.email}`}>{about.email}</a></td>
                </tr>
              </tbody>
            </table>
            <br />
              <div className="links">{about.links.map((link, i) => (
                <a
                  key={`${i}-{link.id}`}
                  href={link.url}
                  target="_blank"
                  rel="noreferrer"
                  className="col-xs-4 col-md-2 social-link"
                >
                  {link.id}
                </a>
              ))}</div>
          </div>
          <Picture skipped={skipTyping} />
        </Pace>
      </WindupChildren>
    </div>
  );
}

export default Header;
