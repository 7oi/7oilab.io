import Header from './Header';
import Picture from './Picture';
import { MyCV } from './MyCv';

export default Header;

export { Header, Picture, MyCV };
