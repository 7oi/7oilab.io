import React, { useState, MouseEventHandler } from 'react';
import { WindupChildren, Pace } from 'windups';
import { MAIN_COLOR } from '../../constants';

interface BitProps {
    onClick ?: MouseEventHandler,
        children ?: string,
        color ?: string
}


function Bit({ onClick, children, color = MAIN_COLOR }: BitProps) {
    const [style, setStyle] = useState({ color });

    const hoverIn = () => {
        const newColor = (Math.random() * 0xFFFFFF << 0).toString(16);
        setStyle({ color: `#${newColor}` });
    }

    const hoverOut = () => {
        setStyle({ color });
    }

    let c = (typeof onClick === 'function') ? onClick : hoverIn;
    return <span onMouseEnter={hoverIn} onMouseLeave={hoverOut} onClick={c} style={style} >{children}</span>
}

interface MyCVProps {
    hiddenFunc: Function,
    skipped?: boolean,
}

// This looks mighty ugly here, but I swear it looks good on screen
// TODO: Rethink:
// - add glint
// - use css more, rather than this whole bit
// - try to randomize the secret, or at least only have one part wrapped in a component
export const MyCV = ({ hiddenFunc, skipped = false }: MyCVProps) => (
    <WindupChildren skipped={skipped} >
        <Pace ms={1}>
            <Bit>    _</Bit><Bit color="magenta">/\/\</Bit><Bit>______</Bit><Bit color="magenta">/\/\</Bit>
            <Bit>__</Bit><Bit color="magenta">/\/\</Bit><Bit>____</Bit><Bit color="magenta">/\/\</Bit>
            <Bit>________</Bit><Bit color="magenta">/\/\/\/\/\</Bit><Bit>__</Bit><Bit color="magenta">/\/\</Bit>
            <Bit>____</Bit><Bit color="magenta">/\/\</Bit><Bit>_</Bit><br />
            <Bit>   _</Bit><Bit color="magenta">/\/\/\</Bit><Bit>__</Bit><Bit color="magenta">/\/\/\</Bit>
            <Bit>__</Bit><Bit color="magenta">/\/\</Bit><Bit>____</Bit><Bit color="magenta">/\/\</Bit>
            <Bit>______</Bit><Bit color="magenta">/\/\</Bit><Bit>__________</Bit><Bit color="magenta">/\/\</Bit>
            <Bit>____</Bit><Bit color="magenta">/\/\</Bit><Bit>_</Bit><br />
            <Bit>  _</Bit><Bit color="magenta">/\/\/\/\/\/\/\</Bit><Bit>____</Bit><Bit color="magenta">/\/\/\/\</Bit>
            <Bit>________</Bit><Bit color="magenta">/\/\</Bit><Bit onClick={(e) => hiddenFunc()}>__________</Bit>
            <Bit color="magenta">/\/\</Bit><Bit>____</Bit><Bit color="magenta">/\/\</Bit><Bit>_</Bit><br />
            <Bit> _</Bit><Bit color="magenta">/\/\</Bit><Bit>__</Bit><Bit color="magenta">/\</Bit><Bit>__</Bit>
            <Bit color="magenta">/\/\</Bit><Bit>______</Bit><Bit color="magenta">/\/\</Bit><Bit>__________</Bit>
            <Bit color="magenta">/\/\</Bit><Bit>____________</Bit><Bit color="magenta">/\/\/\/\</Bit><Bit>___</Bit><br />
            <Bit>_</Bit><Bit color="magenta">/\/\</Bit><Bit>______</Bit><Bit color="magenta">/\/\</Bit><Bit>______</Bit>
            <Bit color="magenta">/\/\</Bit><Bit>____________</Bit><Bit color="magenta">/\/\/\/\/\</Bit><Bit>______</Bit>
            <Bit color="magenta">/\/\</Bit><Bit>_____</Bit><br /><br />
        </Pace>
    </WindupChildren>
);
