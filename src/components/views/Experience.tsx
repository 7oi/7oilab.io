import React, { useContext } from 'react';
import { Pace, Pause, WindupChildren } from 'windups';
import Sample from '../Sample';
import {workExperience, musicExperience} from '../../data';
import { AppContext } from '../../context';
import { MusicExperiences } from '../../typing/pages';
import { useLocation } from 'react-router';

function Experience() {
	const { state } = useContext(AppContext);
	const { command } = state;
	const location = useLocation();
	const subPath: string = location.pathname.split('/')[2];

	if (subPath === 'work') {
		return (
			<WindupChildren>
				<Pace ms={2}>
					<table className="ascii-table">
						{workExperience.map(x => (
							<tbody key={x.job.title}>
								<tr><td><hr /></td><td></td></tr>
								<tr>
									<td>Period      : </td>
									<td>{x.period.start} to {x.period.end}</td>
								</tr>
								<tr>
									<td>Company     : </td>
									<td><a href={x.company.url}>{x.company.name}</a></td>
								</tr>
								<tr>
									<td>Job title   : </td>
									<td>{x.job.title}</td>
								</tr>
								<tr>
									<td>Description : </td>
									<td>{x.job.description}</td>
								</tr>
								<Pause ms={500} />
							</tbody>
						))}
					</table>
				</Pace>
			</WindupChildren>
		)
	}
	return (
		<WindupChildren>
			<Pace ms={2}>
				<table className="ascii-table">
					{musicExperience[command.cmd as keyof MusicExperiences]?.map(x => (
						<tbody key={x.id}>
							<tr><td><hr /></td><td></td></tr>
							<tr>
								<td>Name        : </td>
								<td>{x.url ? <a href={x.url} target="_blank" rel="noreferrer">{x.id}</a> : x.id}</td>
							</tr>
							<tr>
								<td>Date        : </td>
								<td>{x.date}</td>
							</tr>
							<tr>
								<td>Place       : </td>
								<td>{x.place}</td>
							</tr>
							<tr>
								<td>Description : </td>
								<td>{x.description}</td>
							</tr>
							<Sample sample={x.sample} />
							<Pause ms={500} />
						</tbody>
					))}
				</table>
			</Pace>
		</WindupChildren>
	);
}

export default Experience;
