import React from 'react';
import Fetch from 'react-fetch';

function Repos(props) {
	let repos = '';
	if (props[0]) {
		repos = Object.keys(props).map((key, i) => {
			let repo = props[key];
			let fork = repo.fork ? 'True': 'False';
			let forkStyle = repo.fork ? {color: 'magenta'}:{color: 'green'};
			return <tbody key={i}>
				<tr>
					<td>Name        : </td>
					<td><a href={repo.html_url}>{repo.name}</a></td>
				</tr>
				<tr>
					<td>Description : </td>
					<td>{repo.description}</td>
				</tr>
				<tr style={forkStyle}>
					<td>Fork        : </td>
					<td>{fork}</td>
				</tr>
				<tr>
					<td colSpan={2}>
						<hr />
					</td>
				</tr>
			</tbody>
		});
	}
	return <table className="ascii-table">{repos}</table>
}

const Repos = () => (
	<Fetch url="https://api.github.com/users/7oi/repos">
		<RepoRender />
	</Fetch>
)
export default Repos;
