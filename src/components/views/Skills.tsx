import React from 'react';
import { Pace, Pause, WindupChildren } from 'windups';

import { skills, skillNote } from '../../data';
import { SkillsList } from '../../typing/pages';
import { useLocation } from 'react-router';
type SkillKey = keyof SkillsList;

const Skills = () => {
	const location = useLocation();
	const skillPath: SkillKey = location.pathname.split('/')[2] as SkillKey;
	if (!skillPath) return null;
	return (
		<WindupChildren>
			<Pace ms={2}>
				<p>{skillNote}</p>
				<table className="ascii-table">
					{skills[skillPath].map(skill => (
						<tbody key={skill.id}>
							<tr><td><hr /></td><td></td></tr>
							<tr>
								<td>Name    : </td>
								<td>{skill.id}</td>
							</tr>
							<tr>
								<td>Score   : </td>
								<td>{skill.score}</td>
							</tr>
							<tr>
								<td>Use     : </td>
								<td>{skill.use}</td>
							</tr>
							<tr>
								<td>Details : </td>
								<td>{skill.details}</td>
							</tr>
							<Pause ms={500} />
						</tbody>
					))}
				</table>
			</Pace>
		</WindupChildren>
	);
};

export default Skills;
