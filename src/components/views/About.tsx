import React from 'react';
import { useLocation } from 'react-router';
import { WindupChildren, Pace, Pause } from 'windups';

import { about } from '../../data';

const About = () => {
	const location = useLocation();
	const subPath: string = location.pathname.split('/')[2];

	if (subPath === 'interests') {
		return (
			<WindupChildren>
				<hr/>
				<Pace ms={2}>
					<table className="ascii-table">
						<tbody>
							{about.interests.map(i => (
								<tr key={i.id} className="underlined">
									<td className="interest">{i.id}   </td>
									<td className="detail">{i.details}</td>
									<Pause ms={500} />
								</tr>
							))}
						</tbody>
					</table>
				</Pace>
			</WindupChildren>
		);
	}
	return (
		<WindupChildren>
			<hr/>
			<Pace ms={2}>
				<div>
					<p className="underlined">About me</p>
					<p>{about.about}</p>
					<hr />
					<p className="underlined">Qualities</p>
					<p>{about.qualities}</p>
					<hr />
				</div>
			</Pace>
		</WindupChildren>
	);
}

export default About;
