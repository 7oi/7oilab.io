import About from "./About";
import Education from "./Education";
import Experience from "./Experience";
import References from './References';
import Skills from "./Skills";

export { About, Education, Experience, References, Skills };
