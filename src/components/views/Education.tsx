import React from 'react';
import { Pace, Pause, WindupChildren } from 'windups';

import { education } from '../../data';

const Education = () => (
	<WindupChildren>
		<Pace ms={2}>
			<table className="ascii-table">
				{education.map(e => (
					<tbody key={e.degree}>
						<tr><td><hr /></td><td></td></tr>
						<tr>
							<td>Degree   : </td>
							<td>{e.degree}</td>
						</tr>
						<tr>
							<td>Program  : </td>
							<td>{e.program}</td>
						</tr>
						<tr>
							<td>School   : </td>
							<td><a href={e.school.url} target="_blank" rel="noreferrer">{e.school.name}</a></td>
						</tr>
						<tr>
							<td>Period   : </td>
							<td>{e.period.start} to {e.period.end}</td>
						</tr>
						<tr>
							<td>Average  : </td>
							<td>{e.average}</td>
						</tr>
						<tr>
							<td>Status   : </td>
							<td>{e.status}</td>
						</tr>
						<tr>
							<td className="final-project underlined">Final project</td>
						</tr>
						<tr>
							<td>Title      : </td>
							<td>{e.final_project.title}</td>
						</tr>
						<tr>
							<td>Grade      : </td>
							<td>{e.final_project.grade}</td>
						</tr>
						<tr>
							<td>Instructor : </td>
							<td>{e.final_project.instructor}</td>
						</tr>
						<tr>
							<td>Details    : </td>
							<td>{e.final_project.description}</td>
						</tr>
						<Pause ms={1000} />
					</tbody>
				))}
			</table>
		</Pace>
	</WindupChildren>
);

export default Education;
