import React from 'react';
import { Pace, Pause, WindupChildren } from 'windups';

const References = () => (
	<WindupChildren>
		<Pace ms={20}>
			<p>Simply contact me and I'll supply you with references.</p>
			<Pause ms={2000} />
			<p>I feel more comfortable with that than adding contact info for other people here on my site.</p>
		</Pace>
	</WindupChildren>
);

export default References;
