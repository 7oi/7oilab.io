import React from 'react';
import { Routes, Route } from 'react-router-dom';
import './App.scss';
import { Cli } from './components/Cli';
import { About, Education, Experience, References, Skills } from './components/views';
import { skills } from './data';
import './Webfonts/stylesheet.scss';

function App() {
  return (
    <div className="App" id="7oiCV">
      <Routes>
        <Route path="/" element={<Cli />} >
          <Route path="about" element={<About />} >
            <Route path="interests" element={<About />} />
          </Route>
          <Route path="education" element={<Education />} />
          <Route path="skills" element={<Skills />}>
            {Object.keys(skills).map((skill) => {
              return <Route path={skill} element={<Skills />} key={skill} />
            })}
          </Route>
          <Route path="experience" element={<Experience />}>
            <Route path="work" element={<Experience />} />
            <Route path="music" element={<Experience />} />
          </Route>
          <Route path="references" element={<References />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
