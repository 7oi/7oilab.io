
export interface Link {
    id: string,
    url: string,
}

export interface Interest {
    id: string,
    details: string,
}

export interface School {
    name: string,
    url: string,
}

export interface Period {
    start: string,
    end?: string,
}

export interface FinalProject {
    title: string,
    description: string,
    instructor: string,
    grade: string,
}

export interface Education {
    degree: string,
    program: string,
    school: School,
    period: Period,
    average: string,
    status: string,
    final_project: FinalProject,
}

export interface Skill {
    id: string,
    score: string,
    use: string,
    details: string,
}

export interface Company {
    name: string,
    url: string,
}

export interface Job {
    title: string,
    description: string,
}

export interface Employment {
    period: Period,
    company: Company,
    job: Job
}

export interface SampleType {
    sample: string,
    type: string,
    name?: string,
}

export interface MusicProject {
    id: string,
    url: string | null,
    date: string,
    place: string,
    description: string,
    sample: SampleType | null,
}

export interface Reference {
    name: string,
    position: string,
    company: Company,
    phone: string,
    email: string,
}
