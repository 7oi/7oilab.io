export interface Command {
    cmd: string,
    info: string,
    path: string
};

export interface CommandMap {
    home: Command[],
    about: Command[],
    skills: Command[],
    education: Command[],
    references: Command[],
    experience: Command[],
    music: Command[],
}
