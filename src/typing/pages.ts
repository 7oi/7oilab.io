// Page types

import { Education, Interest, Link, MusicProject, Skill } from "./interfaces";

export interface AboutPage {
    name: string,
    phone: string,
    email: string,
    about: string,
    qualities: string,
    status: string,
    links: Link[],
    interests: Interest[],
}

export interface EducationPage {
    list: Education[],
}

export interface SkillsList {
    programming: Skill[],
    tools: Skill[],
    os: Skill[],
    software: Skill[],
}

export interface MusicExperiences {
    theater: MusicProject[],
    film: MusicProject[],
    recordings: MusicProject[],
    oddities: MusicProject[],
}
